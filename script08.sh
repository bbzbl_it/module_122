if [ -w $1 ]
then
    rm $1
else
    echo "Die Datei " $1 " kann nicht gelöscht werden (kein Schreibrecht)."
fi
