if [ $1 -gt $2 ]
then
    echo Die Zahl $1 ist grösser als $2
fi

if [ $2 -gt $1 ]
then
    echo Die Zahl $2 ist grösser als $1
fi

if [ $1 -eq $2 ]
then
    echo Die Zahl $1 ist gleich gross wie $2
fi
