if [ -d "$1" ]
then
    echo "$1" " ist ein Verzeichnis"
else
    if [ -e "$1" ]
    then
        echo "Die Datei " $1 " existiert und hat"
        if [ -r "$1" ]
        then
            echo "Leserecht"
        fi
        
        if [ -w "$1" ]
        then
            echo "Schreibrecht"
        fi
        
        if [ -x "$1" ]
        then
            echo "Ausführungsrecht"
        fi
    else
        echo "Es gibt weder einen Ordner noch eine Datei mit dem Namen " $1
    fi
fi