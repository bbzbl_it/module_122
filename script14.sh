cd "$1"
for item in *; do
    if [ -d $item ]; then
        break
    fi
    
    echo "Current file to process: $item"
    echo "What do you want to do with it?"
    echo "D - Delete File"
    echo "C - Copy File"
    echo "N - No Action"
    read action

    case $action in
    d|D)
        rm "$item"; echo Remove $item;;
    c|C)
        cp "$item" "$item"_copy; echo Copy $item to "$item"_copy;;
    *)
        echo Do nothing with $item;;
    esac
    echo
done