# Code repository for Module 122

## Structure
```
Modul_112
├── README.md
├── backupscript.sh
├── ggt.sh
├── header.txt
├── script01.sh
├── script02.sh
├── script03.sh
├── script04.sh
├── script05.sh
├── script06.sh
├── script07.sh
├── script08.sh
├── script09.sh
├── script10.sh
├── script11.sh
├── script12.sh
├── script13.sh
├── script14.sh
├── script15.sh
├── script16.sh
├── script17.sh
├── script18.sh
├── script19.sh
└── script20.sh
```

## Run code

To run code you first have to enter into a directory with code.

In this case is the current directory already the right one.

Then you can run the code directly:

```
./scriptXX.sh
```
