# einer Variable einen Wert zuweisen
meineVariable="Hallo Basel"

# den Wert einer Variablen ausgeben
echo $meineVariable
echo ${meineVariable}

# den Wert der Umgebungsvariablen PATH ausgeben
echo $PATH

# ersten Parameter ausgeben
echo $1
#zweiten Parameter ausgeben
echo $2
# zehnten Parameter ausgeben
echo ${10}