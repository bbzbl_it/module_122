#!/bin/bash
# Name:         <Name des Scripts>
# Aufruf:       <Name des Scripts> [<1. Parameter>] [<2. Parameter>]
#               [<Beschreibung des 1. Parameters (falls vorhanden)>]
#               [<Beschreibung des 2. Parameters (falls vorhanden)>]
# Beschreibung: <Was macht das Script?>
# Autor:        <Ihr Name>
# Version:      <Versionsnummer>
# Datum:        <Änderungsdatum>

# Parameter in Variablen umkopieren
zahlA=$1
zahlB=$2

# Schleife, solange die beiden Zahlen nicht gleich sind
while [ $zahlA -ne $zahlB ]
do
    # wenn A grösser ist als B
    if [ $zahlA -gt $zahlB ]
    then
        # rechne A = A - B
        let zahlA=$zahlA-$zahlB
    else
        # sonst B = B - A
        let zahlB=$zahlB-$zahlA
    fi
done

# Ergebnis ausgeben
echo Der GGT von $1 und $2 ist $zahlA
